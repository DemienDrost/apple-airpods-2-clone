import Hero from './sections/Hero/Hero'
import { Xray } from './sections/Xray'

function App() {
  return (
    <>
      <Hero />
      <Xray />
      <div style={{outline: "1px solid red", height: "200vh"}}/>
    </>
  )
}

export default App
