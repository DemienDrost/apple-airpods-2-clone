import { useCallback, useEffect, useRef, useState } from "react";
import { StickyContainer, Sticky } from "../../components/Sticky";
import style from "./Hero.module.scss";
import useImagePreloader from "../../hooks/useImagePreloader";
import { animated, useSpring } from "@react-spring/web";

const Hero = () => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [canvas, setCanvas] = useState<HTMLCanvasElement | null>(null);
  const [percentageProgressed, setPercentageProgressed] = useState(0);
  const [fadeOutScale, setFadeOutScale] = useState(0);
  const [fadeOutOpacity, setFadeOutOpacity] = useState(1);
  const [preloadComplete, setPreloadComplete] = useState(false);
  const { preload } = useImagePreloader();
  const [heroStyles, setHeroStyles] = useState<React.CSSProperties>({});

  useEffect(() => {
    if (preload && !preloadComplete) {
      const images = Array(65)
        .fill(null)
        .map((_, index) => {
          const frameFileName = index.toString().padStart(4, "0");
          return `/assets/hero/large/${frameFileName}.png`;
        });
      preload(images);
      setPreloadComplete(true);
    }
  }, [preload, preloadComplete]);

  const drawLayer = useCallback(() => {
    if (!canvas) return;

    const context = canvas.getContext("2d");

    if (!context) return;

    const image = new Image();
    const range = 65;

    let frame = Math.floor(percentageProgressed * range) - 1;
    frame = Math.max(0, Math.min(frame, range - 1));
    const frameFileName = frame.toString().padStart(4, "0");

    image.src = `/assets/hero/large/${frameFileName}.png`;

    image.onload = () => {
      context.resetTransform();
      context.clearRect(0, 0, canvas.width, canvas.height);
      context.drawImage(image, 0, 0);
    };
  }, [canvas, percentageProgressed]);

  useEffect(() => {
    if (canvas == null && canvasRef.current != null) {
      setCanvas(canvasRef.current);
    }

    drawLayer();
  }, [canvas, drawLayer]);

  useEffect(() => {
    drawLayer();

    if (percentageProgressed > 0.05) {
      const newScale = percentageProgressed - 0.05;
      setFadeOutScale(newScale / 2 + 1);
    } else {
      setFadeOutScale(0);
    }

    if (percentageProgressed > 0.7) {
      const newOpacity = 1 - (percentageProgressed - 0.7) * 10;
      setFadeOutOpacity(newOpacity);
    } else {
      setFadeOutOpacity(1);
    }

    setHeroStyles((heroStyles) => ({
      ...heroStyles,
      opacity: fadeOutOpacity < 1 ? fadeOutOpacity : "inherit",
      scale: fadeOutScale > 1 ? fadeOutScale : "inherit",
    }));

    if (fadeOutScale > 1) {
      setHeroStyles((heroStyles) => ({
        ...heroStyles,
        animation: "none"
      }));
    }
  }, [drawLayer, fadeOutOpacity, fadeOutScale, percentageProgressed]);

  const spring = useSpring({
    from: {
      opacity: 1,
      scale: 1,
    },
    to: {
      ...heroStyles
    }
  })

  return (
    <StickyContainer
      height={"300vh"}
      percentageScrolledCallback={(value) => setPercentageProgressed(value)}
    >
      <Sticky className={style.sticky}>
        <div className="hero">
          <div className="lockup">
            <div className="inner">
              <animated.h1 className="hero" style={spring}>
                AirPods Pro
              </animated.h1>
            </div>
          </div>
        </div>
        <div className={style.imageSequence}>
          <div className="hero">
            <canvas ref={canvasRef} width={1440} height={810} />
          </div>
        </div>
      </Sticky>
    </StickyContainer>
  );
};

export default Hero;
