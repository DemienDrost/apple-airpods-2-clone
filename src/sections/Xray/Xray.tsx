import { useEffect, useRef, useState } from "react";
import { Sticky, StickyContainer } from "../../components/Sticky";
import style from "./Xray.module.scss";
import useAnimationHelpers from "../../hooks/useAnimationHelpers";

type AnimationType = Array<{
  name: string;
  start: number;
  end: number;
  ref: React.RefObject<HTMLDivElement>;
  keyframes: {
    [key: string]: {
      [key: string]: number | string;
    };
  };
}>;

export const Xray = () => {
  const [percentageProgressed, setPercentageProgressed] = useState(0);
  const [videoLoaded, setVideoLoaded] = useState(false);

  const { getStyle } = useAnimationHelpers();

  const copy1Ref = useRef<HTMLDivElement>(null);
  const copy2Ref = useRef<HTMLDivElement>(null);
  const copy3Ref = useRef<HTMLDivElement>(null);

  const xrayAirpod1Ref = useRef<HTMLDivElement>(null);
  const xrayAirpod2Ref = useRef<HTMLDivElement>(null);
  const xrayAirpod3Ref = useRef<HTMLDivElement>(null);

  const videoRef = useRef<HTMLVideoElement>(null);
  const particleContainerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const animations: AnimationType = [
      {
        name: "copy 1",
        start: 0.05,
        end: 0.24,
        ref: copy1Ref,
        keyframes: {
          0: {
            opacity: 0,
            y: 100,
          },
          30: {
            opacity: 1,
            y: 50,
          },
          70: {
            opacity: 1,
            y: -50,
          },
          100: {
            opacity: 0,
            y: -100,
          },
        },
      },
      {
        name: "copy 2",
        start: 0.25,
        end: 0.48,
        ref: copy2Ref,
        keyframes: {
          0: {
            opacity: 0,
            y: 100,
          },
          30: {
            opacity: 1,
            y: 50,
          },
          70: {
            opacity: 1,
            y: -50,
          },
          100: {
            opacity: 0,
            y: -100,
          },
        },
      },
      {
        name: "Guts 1",
        start: 0,
        end: 0.48,
        ref: xrayAirpod1Ref,
        keyframes: {
          0: {
            opacity: 1,
            scale: 1,
          },
          95: {
            opacity: 1,
            scale: 0.9,
          },
          100: {
            opacity: 0,
            scale: 0.9,
          },
        },
      },
      {
        name: "Copy 3",
        start: 0.49,
        end: 0.75,
        ref: copy3Ref,
        keyframes: {
          0: {
            opacity: 0,
            y: 100,
          },
          30: {
            opacity: 1,
            y: 50,
          },
          70: {
            opacity: 1,
            y: -50,
          },
          100: {
            opacity: 0,
            y: -100,
          },
        },
      },
      {
        name: "Guts 2",
        start: 0.49,
        end: 0.9,
        ref: xrayAirpod2Ref,
        keyframes: {
          0: {
            opacity: 0,
            scale: 1,
          },
          5: {
            opacity: 1,
            scale: 1,
          },
          100: {
            opacity: 0,
            scale: 0.9,
          },
        },
      },
      {
        name: "Guts 3",
        start: 0.7,
        end: 1,
        ref: xrayAirpod3Ref,
        keyframes: {
          0: {
            opacity: 0,
            scale: 0.9,
          },
          20: {
            opacity: 1,
            scale: 0.9,
          },
          100: {
            opacity: 1,
            scale: 0.8,
          },
        },
      },
      {
        name: "Particles",
        start: 0.7,
        end: 1,
        ref: particleContainerRef,
        keyframes: {
          0: {
            opacity: 0,
          },
          10: {
            opacity: 1,
          },
          100: {
            opacity: 1,
          },
        },
      },
    ];

    animations.forEach((animation) => {
      if (
        percentageProgressed >= animation.start &&
        percentageProgressed <= animation.end
      ) {
        const maxRange = animation.end - animation.start;
        const internalProgress =
          (percentageProgressed - animation.start) / maxRange;

        const nextKeyframeIndex = Object.keys(animation.keyframes).findIndex(
          (keyframe) => Number(keyframe) > internalProgress * 100
        );

        if (nextKeyframeIndex === 0 || nextKeyframeIndex === undefined) return;

        const previousKeyframeIndex = nextKeyframeIndex - 1;

        if (previousKeyframeIndex < 0 || previousKeyframeIndex === undefined)
          return;

        const keys = Object.keys(animation.keyframes) as unknown as Array<
          keyof typeof animation.keyframes
        >;

        const nextKeyframe = animation.keyframes[keys[nextKeyframeIndex]];
        const previousKeyframe =
          animation.keyframes[keys[previousKeyframeIndex]];

        const animate = Object.keys(nextKeyframe ?? -1).map((key) => {
          if (!nextKeyframe || !previousKeyframe) return;

          const property = key as keyof typeof nextKeyframe;
          const nextValue = nextKeyframe[property];
          const previousValue = previousKeyframe[property];

          const previousFrameStart = Number(keys[previousKeyframeIndex]);
          const previousFrameEnd = Number(keys[nextKeyframeIndex]);
          const maxFrameRange = previousFrameEnd - previousFrameStart;
          const internalFrameProgress =
            (internalProgress * 100 - previousFrameStart) / maxFrameRange;

          if (
            typeof nextValue === "number" &&
            typeof previousValue === "number"
          ) {
            const value =
              (nextValue - previousValue) * internalFrameProgress +
              previousValue;
            return [property, value];
          }

          return [property, nextValue];
        });

        animate.forEach((keyframe) => {
          if (!keyframe) return;

          const [property, value] = getStyle(
            keyframe[0].toString(),
            keyframe[1]
          );

          animation.ref.current?.style.setProperty(
            property.toString(),
            value.toString()
          );
        });
      } else if (
        percentageProgressed < animation.start ||
        percentageProgressed > animation.end
      ) {
        let keyframe = animation.keyframes[Object.keys(animation.keyframes)[0]];
        if (percentageProgressed > animation.end) {
          const keys = Object.keys(animation.keyframes);
          keyframe = animation.keyframes[keys[keys.length - 1]];
        }

        Object.keys(keyframe).forEach((key) => {
          const property = key as keyof typeof keyframe;
          const value = keyframe[property];

          const [propertyString, valueString] = getStyle(
            property.toString(),
            value
          );

          animation.ref.current?.style.setProperty(
            propertyString.toString(),
            valueString.toString()
          );
        });
      }
    });
  }, [getStyle, percentageProgressed]);

  useEffect(() => {
    if (videoRef.current == null) return;

    const ref = videoRef.current;

    const handleLoad = () => {
      setVideoLoaded(true);
      ref.play();
      ref.removeEventListener("loadeddata", handleLoad);
    };

    ref.addEventListener("loadeddata", handleLoad);

    return () => {
      ref.removeEventListener("loadeddata", handleLoad);
    };
  }, [videoRef, videoLoaded]);

  return (
    <StickyContainer
      height={"300vh"}
      percentageScrolledCallback={(value) => setPercentageProgressed(value)}
    >
      <Sticky className={style.xraySticky}>
        <div className="lockup-container">
          <div className="lockup">
            <div className="copy guts-1" ref={copy1Ref}>
              <p>
                The chip uses powerful <strong>adaptation algorithms</strong> to
                process sound more quickly, tuning audio at the precise moment
                you hear it. Every detail is rendered for your specific ear
                shape, immersing you in{" "}
                <strong className="green-text">
                  higher‑fidelity&nbsp;sound
                </strong>
                .
              </p>
            </div>
            <div className="copy guts-2" ref={copy2Ref}>
              <p>
                <strong>An inward-facing microphone</strong> works with voice
                enhancement algorithms to recognize and articulate your voice,
                so your phone and video calls always{" "}
                <span className="green-text">sound completely natural</span>.
              </p>
            </div>
            <div className="xray-airpod xray-airpod-1" ref={xrayAirpod1Ref}>
              <img
                src="/assets/airpods/overview/audio_airpod_guts_top__d2is2iiz9l6q_large.jpg"
                alt=""
              />
            </div>
          </div>
          <div className="lockup">
            <div className="copy guts-3" ref={copy3Ref}>
              <p>
                <strong>A custom-built driver and amplifier</strong> work with
                the <strong className="green-text">H2&nbsp;chip</strong> to
                provide lower distortion during playback, so you’ll hear deeper
                bass and crisper highs — across all volume levels.
              </p>
            </div>
            <div className="xray-airpod xray-airpod-2" ref={xrayAirpod2Ref}>
              <img
                src="/assets/airpods/overview/audio_airpod_guts_bottom__bfet1uy1eg0y_large.jpg"
                alt=""
              />
            </div>
          </div>
        </div>
        <div className="lockup-particles">
          <div className="particles-container" ref={particleContainerRef}>
            <div
              className="particles-endframe"
              style={videoLoaded ? { display: "none" } : {}}
            >
              <img
                src="/assets/airpods/overview/noise_reduction_endframe__cc0juowjdaia_large.jpg"
                alt=""
              />
            </div>
            <video
              ref={videoRef}
              className="particles-video"
              muted
              playsInline
              loop
              style={!videoLoaded ? { display: "none" } : {}}
              src="/assets/anim/noise-reduction/large.mp4"
            />
          </div>
          <div className="xray-airpod xray-airpod-3" ref={xrayAirpod3Ref}>
            <img
              src="/assets/airpods/overview/audio_airpod__dni8q27q7tqq_large.jpg"
              alt=""
            />
          </div>
        </div>
      </Sticky>
    </StickyContainer>
  );
};
