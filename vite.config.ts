import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  base: "https://apple-airpods-2-clone-demiendrost-ccc16859a4798fa48a0e072a09d47.gitlab.io/"
})
